###############################################################################
# Standalone Yocto build for imx8mpevk + Basler Camera

Instructions to replicate the build for the imx8mpevk with Basler.

Basler firware is taken from release 1.3.1.9 for imx8mp.


###############################################################################
## Yocto setup and image build

1) create main directory for your yocto work
any name is ok, here I will use MAIN_DIR=imx-5.4.70-2.3.4-basler
$ mkdir imx-5.4.70-2.3.4-basler

2) cd to MAIN_DIR and check out NXP's yocto BSP release 5.4.70-2.3.4
$ cd imx-5.4.70-2.3.4-basler
$ repo init -u https://source.codeaurora.org/external/imx/imx-manifest -b imx-linux-zeus -m imx-5.4.70-2.3.4.xml
$ repo sync

3) copy the provided folder meta-datalogic-basler in <MAIN_DIR>/sources/ you should have the following folders in <MAIN_DIR>/sources/
$ ls -la sources/
base
meta-browser
meta-clang
meta-datalogic-basler (NEW)
meta-freescale
meta-freescale-3rdparty
meta-freescale-distro
meta-imx
meta-nxp-demo-experience
meta-openembedded
meta-python2
meta-qt5
meta-rust
meta-timesys
poky

4) To add docker support (optional)
go to the sources dir and clone the meta-virtualization layer, using the zeus branch
$ <MAIN_DIR>/sources/
$ git clone https://git.yoctoproject.org/git/meta-virtualization
$ cd meta-virtualization
$ git checkout zeus

5) link and run the provided environment setup script.
(if you are not already in the MAIN_DIR, cd to MAIN_DIR)
$ ln -sf sources/meta-datalogic-basler/scripts/env_imx8mpevk.sh env_imx8mpevk.sh
$ source ./env_imx8mpevk.sh

the first time this will perform:
$ DISTRO=fsl-imx-wayland MACHINE=imx8mpevk source imx-setup-release.sh -b imx8mpevk
and perform the first setup of the build environment.
It will also add the meta-datalogic-basler layer to the bblayers.

the next times it will just run
$ source ./setup-environment imx8mpevk
to use the environment already set up.

6) To add docker to image (optional)
Add the followig line to conf/bblayers.conf
BBLAYERS += " ${BSPDIR}/sources/meta-virtualization "

Add the following lines to conf/local.conf
DISTRO_FEATURES_append = " virtualization"
IMAGE_INSTALL_append = " docker"

7) build image
$ bitbake dl-img-imx8mpevk

8) build SDK
$ bitbake dl-img-imx8mpevk -c populate_sdk

###############################################################################
## Ethernet setup (installnet recipe)

The device has default configuration to use dhcp and get an ip address. ssh is enabled and the root user does not have a password.

The default eth configuration is stored in /etc/systemd/network/, where there are two scripts: eth0-dhcp (defult), eth0-static. The currently executed script is the one linked in 10-wired.network. The default configuration is to use eth0-dhcp. To switch to a static ip, you can do the following:

[device console]
$ cd /etc/systemd/network/
(if needed, edit eth0-static ad change ip, default is 10.0.0.2/24)
$ ln -sf eth0-static 10-wired.network
$ sync
$ systemctl restart systemd-networkd.service

The change is permanent and will be applied at each boot.
