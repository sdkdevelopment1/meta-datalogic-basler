SUMMARY = "V4L2Loopback"
DESCRIPTION = "v4l2loopback module"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"


SRC_URI = "git://github.com/umlaeute/v4l2loopback.git;protocol=http;tag=v${PV}"

# Make sure our source directory (for the build) matches the directory structure in the tarball
S = "${WORKDIR}/${PN}-${PV}"

inherit module

KERNEL_MODULE_AUTOLOAD = "kernel-module-v4l2loopback"
