SUMMARY = "Multi Onvif Rtsp Server for Fresco Scanner"
DESCRIPTION = "This recipe is used to build Multi Onvif RTSP Server used in Fresco Scanner for imx8mp-lpddr4-evk board"
SECTION = "multimedia"

SRCREV = "dd0ee09da95f83270ae9fd298d5fa6970adfe238"
SRC_URI = "git://bitbucket.org/sdkdevelopment1/happytime-onvif-client-code.git;protocol=http"

LICENSE = "closed"

inherit pkgconfig

DEPENDS = "ffmpeg boost openssl zlib"

RDEPENDS_${PN} = "x265 x264 opus jpeg libpng"

S = ${WORKDIR}/git

do_compile() {
	oe_runmake -C ${S}/source
}

do_install() {
	install -d ${D}${bindir}
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/fresco-server
	install -d ${D}${sysconfdir}/ssl
	install -d ${D}${sysconfdir}/ssl/certs
	install -d ${D}${sysconfdir}/ssl/private

	install -m 0755 ${S}/source/multionvifrtspserver ${D}${bindir}
	install -m 0755 ${S}/source/start*.sh ${D}${bindir}
	install -m 0755 ${S}/source/stop*.sh ${D}${bindir}
	install -m 0644 ${S}/source/onvif.cfg ${D}${sysconfdir}/fresco-server
	install -m 0644 ${S}/source/rtsp.cfg ${D}${sysconfdir}/fresco-server
	install -m 0644 ${S}/source/ssl.ca ${D}${sysconfdir}/ssl/certs
	install -m 0600 ${S}/source/ssl.key ${D}${sysconfdir}/ssl/private
}

FILES_${PN} =  "${bindir}/* \
		${sysconfdir}/* \
		"
		
	
