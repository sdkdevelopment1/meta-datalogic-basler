SUMMARY = "Basic TCP/IP networking init scripts and configuration files"
DESCRIPTION = "This package provides high level tools to configure network interfaces"
HOMEPAGE = "http://packages.debian.org/ifupdown"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI += "file://eth0-dhcp"
SRC_URI += "file://eth0-static"

S = "${WORKDIR}"

do_install () {
	install -d ${D}${sysconfdir}/systemd/network
	install -m 0644 ${WORKDIR}/eth0-dhcp ${D}${sysconfdir}/systemd/network/eth0-dhcp
	install -m 0644 ${WORKDIR}/eth0-static ${D}${sysconfdir}/systemd/network/eth0-static

	cd ${D}${sysconfdir}/systemd/network
	ln -s eth0-dhcp 10-wired.network
}
