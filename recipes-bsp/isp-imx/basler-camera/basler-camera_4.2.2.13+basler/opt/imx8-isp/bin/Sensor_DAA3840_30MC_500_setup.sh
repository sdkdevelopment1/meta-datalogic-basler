#! /bin/bash
# (c) Basler AG 2021

RUN_SCRIPT=`realpath -s $0`
RUN_SCRIPT_PATH=`dirname $RUN_SCRIPT`


# mode mapping for 500MHz pixel clock on daA3840-30mc
ISP_INDEX=$1

# default value if no override set in /etc/default/imx8-isp
MODE_DAA3840_30MC_500=${MODE_DAA3840_30MC_500:-4K30}

# map mode name to code
case ${MODE_DAA3840_30MC_500} in
    '1080P60' )
        MODE=1
        ;;
    '4K30' )
        MODE=0
        ;;
    '1080P60_HDR' )
        MODE=3
        ;;
    '4K30_HDR' )
        MODE=2
        ;;
    * )
        echo "Invalid camera mode ${MODE_DAA3840_30MC_500}"
        exit 1
        ;;
esac

# create final sensor entry file
cat ${RUN_SCRIPT_PATH}/Sensor_DAA3840_30MC.cfg | sed "s/\*SENSOR_MODE\*/$MODE/" > ${RUN_SCRIPT_PATH}/Sensor${ISP_INDEX}_Entry.cfg
