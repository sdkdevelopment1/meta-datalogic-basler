#! /bin/bash
# (c) Basler AG 2021

RUN_SCRIPT=`realpath -s $0`
RUN_SCRIPT_PATH=`dirname $RUN_SCRIPT`

# mode mapping for 266MHz pixel clock on daA2500-60mc
ISP_INDEX=$1

# default value if no override set in /etc/default/imx8-isp
MODE_DAA2500_60MC_266=${MODE_DAA2500_60MC_266:-1080P60}

# map mode name to code
MODE=""
case ${MODE_DAA2500_60MC_266} in
    '1080P60' )
        MODE=1
        ;;
    * )
        echo "Unknown camera mode ${MODE_DAA2500_60MC_266}"
        exit 1
        ;;
esac

# create final sensor entry file
cat ${RUN_SCRIPT_PATH}/Sensor_DAA2500_60MC.cfg | sed "s/\*SENSOR_MODE\*/$MODE/" > ${RUN_SCRIPT_PATH}/Sensor${ISP_INDEX}_Entry.cfg
