# Copyright 2021 Basler
# Released under the MIT license (see COPYING.MIT for the terms)

# override NXP definition to not include dev packages
ISP_PKGS = " \
    isp-imx \
    basler-camera \
    kernel-module-isp-vvcam \
"
