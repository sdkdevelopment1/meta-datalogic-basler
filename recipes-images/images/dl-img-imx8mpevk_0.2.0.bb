# image for imx8mmpevk

DESCRIPTION = "Image for imx8mpevk with basler camera support"

inherit core-image

IMAGE_FEATURES += " \
    ssh-server-openssh \
    hwcodecs \
"

# base tools and utils
IMAGE_INSTALL:append = " \
    packagegroup-base \
    packagegroup-core-boot \
    packagegroup-core-base-utils \
    packagegroup-imx-core-tools \
    packagegroup-imx-security \
    libgcc libstdc++ \
    dhcpcd \
    mmc-utils \
    usbutils libusb1 \
    parted \
    util-linux \
    procps \
    libcap-bin \
    installnet \
    packagegroup-fsl-gstreamer1.0 \
    packagegroup-fsl-gstreamer1.0-full \
"
# fresco-server and utilities
IMAGE_INSTALL:append = " \
    fresco-server \
    kernel-module-v4l2loopback \
    v4l-utils \
    ffmpeg \
"

# basler camera support
IMAGE_INSTALL:append = " \
    gentl-producer \
    kernel-module-basler-camera \
    imx8mp-modprobe-config \
    kernel-module-isp-vvcam \
    isp-imx \
    basler-camera \
"

# ML packages
IMAGE_INSTALL:append = " \
    armnn \
    tensorflow-lite \
    onnxruntime \
	python3-opencv \
	python3-imaging \
"

# Docker: to add docker uncomment the following line:
# IMAGE_INSTALL:append = " docker"

# toolchain
TOOLCHAIN_TARGET_TASK:append += " \
	tensorflow-lite-staticdev \
	tensorflow-lite-dev \
	armnn-dev \
	onnxruntime-dev \
	libstdc++-staticdev \
"

export IMAGE_BASENAME = "dl-img-imx8mpevk"
