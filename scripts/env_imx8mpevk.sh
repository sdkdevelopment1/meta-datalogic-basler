BUILD_DIR="./imx8mpevk"

_MACHINE=imx8mp-lpddr4-evk
_DISTRO=fsl-imx-wayland

EULA=1

if [[ -d ${BUILD_DIR} ]]; then
  source ./setup-environment ${BUILD_DIR}
else
  MACHINE=${_MACHINE}
  DISTRO=${_DISTRO}
  source ./imx-setup-release.sh -b ${BUILD_DIR}
fi

# setup to be run only the first time
if [[ ! -e ./conf/setupdone ]]; then

	echo "Running environment first setup."

	# add datalogic layers
	if ! grep -q datalogic ./conf/bblayers.conf; then
		echo "BBLAYERS += \" \${BSPDIR}/sources/meta-datalogic-basler \"" >> ./conf/bblayers.conf
	fi

	# TODO: check license status
	# add commenrcial license for ffmpeg
	echo "" >> ./conf/local.conf
	echo "# Permit commericl codec use" >> ./conf/local.conf
	echo "LICENSE_FLAGS_WHITELIST += \"commercial_ffmpeg commercial_x264 commercial\"" >> ./conf/local.conf

	echo "" >> ./conf/local.conf
	echo "# Set preferred version for basler drivers" >> ./conf/local.conf
	echo "PREFERRED_VERSION_isp-imx = \"4.2.2.13+basler\"" >> ./conf/local.conf
	echo "PREFERRED_VERSION_basler-camera = \"4.2.2.13+basler\"" >> ./conf/local.conf
	echo "PREFERRED_VERSION_kernel-module-isp-vvcam = \"4.2.2.13+basler\"" >> ./conf/local.conf

	echo "" >> ./conf/local.conf
	echo "# To save space remove work once done, keeping only the main components" >> ./conf/local.conf
	echo "INHERIT += \"rm_work\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" kernel-module-basler-camera\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" imx8mp-modprobe-config\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" kernel-module-isp-vvcam\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" isp-imx\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" basler-camera\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" linux-imx\"" >> ./conf/local.conf
	echo "RM_WORK_EXCLUDE += \" u-boot-imx\"" >> ./conf/local.conf

	echo "1" > ./conf/setupdone

	echo "Done"
	echo ""
fi
